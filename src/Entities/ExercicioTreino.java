package Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import DAO.EntidadeBase;

@Entity
public class ExercicioTreino implements EntidadeBase{
	@Id
	@GeneratedValue
	Long exercicioTreino;
	
	@ManyToOne
	Treino treino;
	
	@OneToOne
	Exercicio exercicio;
	
	String nome;
	String tipoExercicio;
	
	
	public Long getExercicioTreino() {
		return exercicioTreino;
	}
	public void setExercicioTreino(Long exercicioTreino) {
		this.exercicioTreino = exercicioTreino;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTipoExercicio() {
		return tipoExercicio;
	}
	public void setTipoExercicio(String tipoExercicio) {
		this.tipoExercicio = tipoExercicio;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return exercicioTreino;
	}
	
	

	

}
