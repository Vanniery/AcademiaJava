package Entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import DAO.EntidadeBase;

@Entity
public class Pagamento implements EntidadeBase{
	
	@Id
	@GeneratedValue
	Long idPagamento;
	
	@OneToOne(mappedBy="pagamento")
	Aluno aluno;
	
	double preco;
	Date dataRecebimento;
	Date dataPagamento;
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return idPagamento;
	}
	
	
	

}
