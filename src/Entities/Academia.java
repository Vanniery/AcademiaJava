package Entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import DAO.EntidadeBase;

@Entity

public class Academia implements EntidadeBase{
	
	@Id
	@GeneratedValue
	Long idAcademia;
	int quantAlunos;
	int quantProfessores;
	String nome;
	String CNPJ;
	
	@ManyToMany(mappedBy="academias")
	Collection<Aluno> alunos;
	
	@ManyToMany
	Collection<Professor> professores;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return idAcademia;
		
	}
	

	
	public Long getIdAcademia() {
		return idAcademia;
	}



	public void setIdAcademia(Long idAcademia) {
		this.idAcademia = idAcademia;
	}



	public int getQuantAlunos() {
		return quantAlunos;
	}

	public void setQuantAlunos(int quantAlunos) {
		this.quantAlunos = quantAlunos;
	}

	public int getQuantProfessores() {
		return quantProfessores;
	}

	public void setQuantProfessores(int quantProfessores) {
		this.quantProfessores = quantProfessores;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}

	public Collection<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(Collection<Aluno> alunos) {
		this.alunos = alunos;
	}

	public Collection<Professor> getProfessores() {
		return professores;
	}

	public void setProfessores(Collection<Professor> professores) {
		this.professores = professores;
	}
	
	
	
	
	
	

}
