package Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import DAO.EntidadeBase;

@Entity
public class Exercicio implements EntidadeBase{
	
	@Id
	@GeneratedValue
	Long IdExercicio;
	
	@OneToOne(mappedBy= "exercicio")
	ExercicioTreino exercicioTreino;
	
	int serie;
	int repeticao;
	
	public Long getIdExercicio() {
		return IdExercicio;
	}
	public void setIdExercicio(Long idExercicio) {
		IdExercicio = idExercicio;
	}
	public int getSerie() {
		return serie;
	}
	public void setSerie(int serie) {
		this.serie = serie;
	}
	public int getRepeticao() {
		return repeticao;
	}
	public void setRepeticao(int repeticao) {
		this.repeticao = repeticao;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return IdExercicio;
	}
	
	

}
