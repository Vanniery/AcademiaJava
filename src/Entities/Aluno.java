package Entities;

import java.util.Collection;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import DAO.EntidadeBase;

@Entity
public class Aluno implements EntidadeBase{

	@Id
	@GeneratedValue
	Long matricula;

	@OneToMany(mappedBy = "aluno")
	Collection<Avaliacao> avalia�oes;

	@OneToOne
	Pagamento pagamento;

	@OneToMany(mappedBy = "aluno")
	Collection<Treino> treinos;
	
	@ManyToMany
	Collection<Academia> academias;

	String nome;
	int idade;
	String endere�o;
	boolean situa��o;// ficou no lugar de ativ
	Date dataInicio;
	String email;

	public Long getMatricula() {
		return matricula;
	}

	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getEndere�o() {
		return endere�o;
	}

	public void setEndere�o(String endere�o) {
		this.endere�o = endere�o;
	}

	public boolean isSitua��o() {
		return situa��o;
	}

	public void setSitua��o(boolean situa��o) {
		this.situa��o = situa��o;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return matricula;
	}

	public Collection<Avaliacao> getAvalia�oes() {
		return avalia�oes;
	}

	public void setAvalia�oes(Collection<Avaliacao> avalia�oes) {
		this.avalia�oes = avalia�oes;
	}

	public Pagamento getPagamento() {
		return pagamento;
	}

	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}

	public Collection<Treino> getTreinos() {
		return treinos;
	}

	public void setTreinos(Collection<Treino> treinos) {
		this.treinos = treinos;
	}

	public Collection<Academia> getAcademias() {
		return academias;
	}

	public void setAcademias(Collection<Academia> academias) {
		this.academias = academias;
	}
	

}
