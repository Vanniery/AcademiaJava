package Entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import DAO.EntidadeBase;

@Entity
public class Treino implements EntidadeBase{

	@Id
	@GeneratedValue
	Long idTreino;

	@ManyToOne
	Aluno aluno;

	@OneToMany(mappedBy = "treino")
	List<ExercicioTreino> exerciciosTreinos;

	String nome;
	String descricao;

	public Long getIdTreino() {
		return idTreino;
	}

	public void setIdTreino(Long idTreino) {
		this.idTreino = idTreino;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<ExercicioTreino> getExerciciosTreinos() {
		return exerciciosTreinos;
	}

	public void setExerciciosTreinos(List<ExercicioTreino> exerciciosTreinos) {
		this.exerciciosTreinos = exerciciosTreinos;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return idTreino;
	}

}
