package Entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import DAO.EntidadeBase;

@Entity

public class Avaliacao implements EntidadeBase {

	@Id
	@GeneratedValue
	Long idAvaliacao;
	
	@ManyToOne
	Aluno aluno;
	
	@ManyToOne
	Professor professor;
	
	Date data;
	double peso ;
	double altura ;
	double IMC;
	double cintura;
	double peito;
	double bracoDireito;
	double bracoEsquerdo;
	double gluteos;
	double coxaEsquerda;
	double coxaDireita;
	double panturrilhaEsquerda;
	double panturrilhaDireita;
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return idAvaliacao;
	}
	
	
	
	
}
