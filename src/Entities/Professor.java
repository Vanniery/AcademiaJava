package Entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import DAO.EntidadeBase;

@Entity
public class Professor implements EntidadeBase{
	
	@Id
	@GeneratedValue
	Long idProfessor;
	
	@OneToMany(mappedBy="professor")
	Collection<Avaliacao> avaliaçoes;
	
	
	@ManyToMany(mappedBy="professores")
	Collection<Academia> academias;
	
	String nome;
	int idade;
	String formacao;
	boolean situacao;//lugar de ativ no diagrama de classe
	
	
	public Long getIdProfessor() {
		return idProfessor;
	}
	public void setIdProfessor(Long idProfessor) {
		this.idProfessor = idProfessor;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getFormacao() {
		return formacao;
	}
	public void setFormacao(String formacao) {
		this.formacao = formacao;
	}
	public boolean isSituacao() {
		return situacao;
	}
	public void setSituacao(boolean situacao) {
		this.situacao = situacao;
	}
	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return idProfessor;
	}
	
}
