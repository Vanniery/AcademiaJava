package DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import Entities.Academia;

public class AcademiaDao extends GenericDao<Academia> {
	
	public List<Academia> getTodasAcademias(int i)
    {
		EntityManager em = getEM();
        Query query = em.createQuery("SELECT t FROM academia t where qualidade="+i);
        List<Academia> t = query.getResultList();
        return t;
    }
}